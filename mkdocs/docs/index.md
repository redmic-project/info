# ¿Qué significa REDMIC?
REDMIC son las siglas de "Repositorio de Datos Marinos Integrados de Canarias", una base de datos geográficos concebida para reunir cualquier información relacionada con el entorno marino. Puede provenir de multitud de orígenes y formatos, de cualquier naturaleza concebible en todos los ámbitos del conocimiento (oceanografía, biología, hidrodinámica, pesca, geología, transporte marítimo, etc.). El repositorio, además de almacenar y mantener publicados los datos, está dotado de la capacidad de integrar la información para poder tratar los datos conjuntamente.

## ¿Otro buscador más?

REDMIC no es otro rastreador más que busca datos recorriendo múltiples webs o IDE, sino que, en contraste, almacena los datos aportados para que luego puedan ser consultados o analizados. Esta estrategia implica un mayor esfuerzo al añadir datos, pero por contra, simplifica la explotación de los mismos. Añadir un dato es una acción que se efectúa una sola vez, pero permite que ese dato sea aprovechado múltiples veces en diferentes estudios o análisis. De esta forma se maximiza la reutilización de los datos.

## ¿No estaba la centralización pasada de moda?

Nuestra estrategia para el análisis de datos recomienda tenerlos un sitio común, con datos homogeneizados para generar mejores informes y de manera más ágil, pudiendo incluso descubrir relaciones entre variables que, de otro modo, serían difíciles de descubrir. En ocasiones tenemos una idea preconcebida sobre qué fenómenos actúan sobre la realidad que se estudia, pero el precio de descartar influencias insospechadas de nuevas variables puede ser muy alto. Por eso, solo con la integración de los datos, es posible tener en cuenta todas las variables que entran en juego en un sistema, maximizando así el potencial de explotación del dato, pero respetando siempre el dato original y las circunstancias en las que se generó, o sea, sus metadatos. 

