# REDMIC para instituciones
## Colaboración con datos

Mediante un convenio de colaboración, cualquier institución interesada puede utilizar REDMIC como repositorio para sus datos, además de poder utilizar todos los servicios que la aplicación ofrece, como cualquier otro usuario. Así, si como institución tienes pensado desarrollar una herramienta para almacenar, analizar y/o publicar tus datos, puedes contactar con nosotros, dónde pensaremos una solución para integrarlos, aprovechando el trabajo ya hecho. Si estás interesado envía un correo electrónico a la dirección [info@redmic.es](mailto:info@redmic.es) y empezaremos las gestiones para establecer la colaboración.

## Colaboración con desarrollo

Si se precisa de una funcionalidad concreta que pueda tener cabida en REDMIC pero aún no existe, estamos abiertos a sugerencias y a realizar un desarrollo colaborativo. Toda aportación que aumente la potencia de la plataforma es bienvenida.

## Aprovechamiento de REDMIC

Para sacar partido a los beneficios que aporta REDMIC no es necesario adoptar toda la plataforma en su conjunto. Por ejemplo, una institución podría hacer uso solamente de un conjunto de los datos que aporta el repositorio, alimentando con ellos a sus propias aplicaciones.

Por otro lado, si una institución desea almacenar sus datos en un repositorio y no posee las herramientas o los medios necesarios, puede hacer uso de REDMIC para almacenar y consumir sus datos, además de disfrutar de todas las ventajas que aporta la plataforma.

## Beneficios

### Visibilidad

Al incorporar tus datos no solo los tendrás localizados y accesibles para los investigadores de tu institución, sino que investigadores de otras instituciones podrán también acceder a ellos.

De esta manera, la institución ganará visibilidad, ya que el resto de la comunidad científica sabrá en qué líneas de investigación están trabajando. Además, se potencia que surjan colaboraciones institucionales.

### Ahorro

Al aprovechar los recursos que ofrece REDMIC, las instituciones pueden ahorrar costes en el desarrollo de soluciones propias.

En el caso de aportar datos, también se ahorrarían costes en infraestructuras para poder almacenar y publicar los mismos. Como consecuencia, también se reduce el coste asociado al mantenimiento.

## Colaboradores

Actualmente existen acuerdos de colaboración con algunas instituciones que han realizado aportaciones a REDMIC. Colabora con nosotros para que esta lista siga creciendo.

![ieo](images/ieo.jpg)
![loro_parque](images/loro_parque.png)
