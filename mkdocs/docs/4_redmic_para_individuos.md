# REDMIC para individuos
## Necesito obtener datos

### Visualización y descarga de datos

Una de las principales funciones de REDMIC es servir al usuario como fuente de datos para sus estudios o investigaciones. Así, cualquier usuario de la aplicación podrá visualizar los datos de diferentes maneras o descargar directamente solo los datos que sean de su interés.

Para visualizar los datos directamente en la aplicación,  se cuenta con un visor en el que el usuario puede consultar los datos en forma de tabla, gráfica o mapa. Dependiendo del tipo de dato será posible su visualización de una forma u otra.

También existen otros métodos para acceder a los datos a través de servicios.

### Formatos

Compartir datos e intercambiar información con otros sistemas o bases de datos es un objetivo fundamental de REDMIC. Por ello, los datos deben estar siempre en formatos estándar, abiertos y aceptados por la comunidad científica y por la mayoría de los usuarios. Así, se han elegido los formatos NetCDF (Network Common Data Format), GeoTIFF, CSV, JSON, GeoJSON o PDF entre otros, por ser los más utilizados en este ámbito. El formato dependerá del tipo de dato implicado.

### Precio

REDMIC es una aplicación totalmente gratuita. Cualquier usuario puede acceder a todos los datos disponibles, así como subir sus datos sin coste alguno. Además puede hacer uso de las herramientas analíticas integradas en la aplicación web, generar informes, etc.

## Tengo o produzco datos

### Incorporar datos

En la actualidad REDMIC se encuentra en desarrollo. Por el momento, la manera de incorporar datos en la aplicación es vía email, a la dirección [info@redmic.es](mailto:info@redmic.es). En un futuro, cada usuario podrá subir sus datos directamente a través de la aplicación.

### Custodia

Cuando un usuario aporta sus datos, no solo los está compartiendo con el resto de usuarios, sino que recibe un servicio que le garantiza que sus datos permanecerán en la aplicación indefinidamente. De esta manera, REDMIC ofrece un servicio de almacenamiento de datos, evitando que se pierdan después de haber sido utilizados. Así se consigue mantener series temporales más largas y continuas, estando siempre disponibles para su consulta. Además de ser custodiados, los datos serán homogeneizados e integrados con todos los demás para aumentar su potencial de uso.

###Registro de propiedad

El usuario depositante de datos concede a REDMIC el permiso de facilitar sus datos a terceros, según la política de datos de la aplicación. A su vez, si así lo desean, podrán recibir un certificado acreditativo, el cual registra que el depositante ha cedido sus datos para beneficio de la comunidad.

### Embargo

El sistema ofrece a los usuarios un mecanismo de embargo temporal de sus datos, para darles tiempo a que los publiquen. De esta manera, los datos estarán en la aplicación pero solo el depositante podrá acceder a ellos, beneficiándose de las herramientas que ofrece REDMIC durante un periodo de tiempo que él mismo fijará. También cabe la opción de que los datos de particulares solo puedan ser descargados una vez obtenido el consentimiento explícito de cesión de uso de datos. La referencia sería visible en el catálogo general, con indicación del contacto a quién pedir la autorización para acceder a ellos.

## Beneficios

### Visibilidad como científico
Cuando un usuario sube datos a la aplicación, el resto de usuarios pueden saber quién es, dónde trabaja, a que se ha dedicado durante los últimos años y cuál es su línea de trabajo actual. De esta manera, si un usuario es experto en un tema, el resto puede localizarle y establecer contacto con él para futuras colaboraciones.

### Visibilidad y accesibilidad de los datos
Los datos disponibles en REDMIC son visibles para la comunidad científica. De este modo, se tiene conocimiento de los datos ya existentes, pudiéndose consultar, descargar y leer los metadatos correspondientes.
De nuevo se favorece la colaboración entre investigadores, a la vez que se intenta que no haya duplicidades en investigaciones, con el consecuente ahorro de dinero.

### Registro de propiedad
Si el depositante lo solicita, se generarán informes de uso por parte de terceros de los datos que ha aportado. De esta manera, podrá estar informado de quién y para qué se están usando esos datos.

Además, también podrá solicitar requerimiento de atribución, es decir, que se debe nombrar la procedencia de los datos en las nuevas publicaciones que hagan uso de estos.

Estas características hacen que aportar datos sea aún más interesante, ya que además de fomentar su reutilización, sirve para que el valor de los datos sea reconocido en sucesivos trabajos.

## ¿Existen otras maneras de colaborar?

Por supuesto. Es posible colaborar de diversas formas, no solo aportando datos:

- **Indicando errores detectados en los datos**. A pesar de que los datos pasan un exhaustivo control de calidad, puede ocurrir que solo cuando se vayan a usar, salga a la luz algún defecto en los mismos. Si eso ocurre, puede contactar con el equipo de REDMIC para dar solución a este problema.
- **Indicando problemas encontrados en la plataforma**. Mediante el uso, tanto de la aplicación web como de los servicios ofrecidos por REDMIC, se pueden detectar problemas puntuales o posibles mejoras. Comentarios y sugerencias serán bien recibidos.
- **Desarrollando o colaborando en la mejora de funcionalidades**. Si descubres funcionalidades no cubiertas por REDMIC y quieres informar sobre ello (o incluso desarrollar dicha funcionalidad), no dudes en contactar con nosotros.

## ¿Es fácil de usar?

En REDMIC somos conscientes de lo complicado que es usar aplicaciones en este ámbito. Existen aplicaciones con caminos interminables para lograr visualizar o descargar los datos, que no distinguen bien entre datos y metadatos, en formatos poco usables o incluso privativos, tiempos de carga demasiado prolongados, paneles de mando que muestran demasiadas opciones al mismo tiempo, etc.

Por ello, se ha puesto el foco en la ergonomía, la facilidad de uso y la accesibilidad, para conseguir una aplicación intuitiva y fácil de manejar para cualquier usuario que esté mínimamente familiarizado con la navegación web.

Se sigue trabajando continuamente para mejorar la experiencia del usuario, y estamos abiertos a sugerencias y propuestas de mejoras en este sentido.

No obstante, se reforzará este aspecto con módulos de ayuda (actualmente en desarrollo) y ejemplos concretos de uso.

